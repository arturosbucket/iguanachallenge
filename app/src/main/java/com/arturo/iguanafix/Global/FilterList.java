package com.arturo.iguanafix.Global;

import com.arturo.iguanafix.Models.Contact;

import java.util.ArrayList;
import java.util.List;

public class FilterList {

    public static List<Contact> getFilteredList(List<Contact> contacts, CharSequence charSequence) {

        List<Contact> contactListFiltered;
        contactListFiltered = new ArrayList<>();

        String charString = charSequence.toString();

        if (charString.isEmpty()) {
            contactListFiltered = contacts;
        } else {

            for (Contact row : contacts) {

                if (row.getFullName().toLowerCase().contains(charString.toLowerCase())) {
                    contactListFiltered.add(row);
                }
            }

        }
        return contactListFiltered;

    }

}
