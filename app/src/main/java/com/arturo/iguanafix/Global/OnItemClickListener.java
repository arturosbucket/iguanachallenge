package com.arturo.iguanafix.Global;

import com.arturo.iguanafix.Models.Contact;

public interface OnItemClickListener {
    void onItemClick(Contact item);
}

