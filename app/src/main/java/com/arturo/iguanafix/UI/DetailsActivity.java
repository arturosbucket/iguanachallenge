package com.arturo.iguanafix.UI;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arturo.iguanafix.API.GlideApp;
import com.arturo.iguanafix.Models.Contact;
import com.arturo.iguanafix.R;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arturo.iguanafix.API.RetrofitClient.retrofitClient;

public class DetailsActivity extends AppCompatActivity {


    @BindView(R.id.constraint)
    ConstraintLayout detailsLayout;
    @BindView(R.id.contact_image)
    ImageView contactImage;
    @BindView(R.id.loading)
    ProgressBar progressBar;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.age)
    TextView age;
    @BindView(R.id.contact_since)
    TextView contactSince;
    @BindView(R.id.home_phone)
    TextView homePhone;
    @BindView(R.id.cell_phone)
    TextView cellPhone;
    @BindView(R.id.work_phone)
    TextView workPhone;
    @BindView(R.id.home_address)
    TextView homeAddress;
    @BindView(R.id.work_address)
    TextView workAdress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        final Contact contact = (Contact) intent.getSerializableExtra("Contact");

        Call<Contact> call = retrofitClient.getContactDetails(contact.getUser_id());

        call.enqueue(new Callback<Contact>() {
            @Override
            public void onResponse(Call<Contact> call, Response<Contact> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.INVISIBLE);
                    detailsLayout.setVisibility(View.VISIBLE);
                    Contact contact = response.body();
                    setData(contact);

                } else {
                    Toast.makeText(DetailsActivity.this, "Error en Conexion", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Contact> call, Throwable t) {
                Toast.makeText(DetailsActivity.this, "Error en Conexion", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void setData(Contact contact) {

        GlideApp.with(this)
                .load(contact.getPhoto())
                .error(GlideApp.with(this)
                        .load(contact.getThumb())
                        .error(R.drawable.silhouette))
                .into(contactImage);

        name.setText(contact.getFullName());
        age.setText("Fecha de Nacimiento: " + contact.getBirth_date());
        contactSince.setText("Agregado el "+contact.getFormattedDate());
        homePhone.setText(contact.getHomePhone());
        cellPhone.setText(contact.getCellPhone());
        workPhone.setText(contact.getWorkPhone());
        homeAddress.setText(contact.getHomeAddress());
        workAdress.setText(contact.getWorkAddress());

    }

}
