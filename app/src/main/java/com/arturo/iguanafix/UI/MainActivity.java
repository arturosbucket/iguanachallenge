package com.arturo.iguanafix.UI;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.arturo.iguanafix.Global.FilterList;
import com.arturo.iguanafix.Global.OnItemClickListener;
import com.arturo.iguanafix.Models.Contact;
import com.arturo.iguanafix.Adapters.ContactAdapter;
import com.arturo.iguanafix.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.arturo.iguanafix.API.RetrofitClient.retrofitClient;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.contact_list)
    RecyclerView contactList;

    @BindView(R.id.loading)
    ProgressBar progressBar;

    @BindView(R.id.search_text)
    EditText searchText;

    List<Contact> contacts;
    List<Contact> contactListFiltered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(MainActivity.this);

        contacts = new ArrayList<>();
        contactListFiltered = new ArrayList<>();
        Call<List<Contact>> call = retrofitClient.getContact();

        call.enqueue(new Callback<List<Contact>>() {
            @Override
            public void onResponse(Call<List<Contact>> call, final Response<List<Contact>> response) {
                if (response.isSuccessful()) {
                    contacts = response.body();
                    setData(contacts);

                    progressBar.setVisibility(View.INVISIBLE);
                    contactList.setVisibility(View.VISIBLE);

                } else {
                    Toast.makeText(MainActivity.this, "Error en Conexion", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Contact>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error en Conexion", Toast.LENGTH_LONG).show();
            }
        });

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                setData(contacts);
                contactList.getAdapter().notifyDataSetChanged();
                Log.d("char entered", "char entered");
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void setData(List<Contact> contacts) {

        List<Contact> filteredList = FilterList.getFilteredList(contacts, searchText.getText().toString());

        contactList.setAdapter(new ContactAdapter(filteredList, new OnItemClickListener() {
            @Override
            public void onItemClick(Contact item) {

                getDetails(item);
            }
        }));
        contactList.setLayoutManager(new LinearLayoutManager(this));
    }

    private void getDetails(Contact contact) {

        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
        intent.putExtra("Contact", contact);
        startActivity(intent);

    }

}
