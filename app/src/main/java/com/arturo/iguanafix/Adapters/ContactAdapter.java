package com.arturo.iguanafix.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arturo.iguanafix.API.GlideApp;
import com.arturo.iguanafix.Global.OnItemClickListener;
import com.arturo.iguanafix.Models.Contact;
import com.arturo.iguanafix.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactHolder> {

    private List<Contact> contacts;
    private OnItemClickListener listener;


    public ContactAdapter(List<Contact> contacts, OnItemClickListener listener) {
        this.contacts = contacts;
        this.listener = listener;
    }

    public static class ContactHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contact_image)
        ImageView contactImage;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.index)
        TextView index;


        public ContactHolder(View itemView) {
            super(itemView);
           ButterKnife.bind(this,itemView);

        }

        public void bind(final Contact contact, final OnItemClickListener listener,int position) {

            index.setText(String.valueOf(position));
            name.setText(contact.getFullName());
            GlideApp.with(itemView.getContext())
                    .load(contact.getThumb())
                    .error(R.drawable.silhouette)
                    .centerCrop()
                    .into(contactImage);

            itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(contact);
                }
            });
        }
    }

    @NonNull
    @Override
    public ContactHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        LinearLayout contactView = (LinearLayout) inflater.inflate(R.layout.contact_layout, parent, false);

        return new ContactHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactHolder holder, int position) {
        holder.bind(contacts.get(position), listener, position);

    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }


}

