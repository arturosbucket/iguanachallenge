package com.arturo.iguanafix.API;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public class GlideAppClient extends AppGlideModule{

    public void setView(Context context, String imageUrl, ImageView image){

        GlideApp.with(context).load(imageUrl).centerCrop().into(image);

    }




}




