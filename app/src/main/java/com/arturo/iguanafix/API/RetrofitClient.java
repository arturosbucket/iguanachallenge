package com.arturo.iguanafix.API;

import com.arturo.iguanafix.Models.Contact;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RetrofitClient {

    String BASE_URL = "https://private-d0cc1-iguanafixtest.apiary-mock.com/";


    @GET("contacts")
    Call<List<Contact>> getContact();

    @GET("contacts/{user}")
    Call<Contact> getContactDetails(@Path("user") String user);

    Retrofit retrofit =
            new Retrofit.Builder()
                    .baseUrl(RetrofitClient.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

    RetrofitClient retrofitClient = retrofit.create(RetrofitClient.class);


}