package com.arturo.iguanafix.Models;

import android.content.res.Resources;
import android.widget.TextView;

import com.arturo.iguanafix.R;
import com.bumptech.glide.load.engine.Resource;

import java.io.Serializable;
import java.util.List;

public class Contact implements Serializable {

    private List<Addresses> addresses;

    private String photo;
    private String thumb;
    private List<Phones> phones;
    private String last_name;
    private String first_name;
    private String birth_date;
    private String created_at;
    private String user_id;

    public List<Addresses> getAddresses() {
        return addresses;
    }

    public String getPhoto() {
        return photo;
    }

    public String getThumb() {
        return thumb;
    }

    public List<Phones> getPhones() {
        return phones;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getFormattedDate() {
        android.text.format.DateFormat df = new android.text.format.DateFormat();
        return df.format("yyyy-MM-dd ", new java.util.Date()).toString();

    }

    public String getFullName() {
        String fullName = (first_name + " " + last_name);
        return fullName;
    }

    public String getHomePhone() {
        String data = "";
        for (Phones phone : phones) {
            if (phone.getType().equals("Home") && phone.getNumber() != null) {
                data = phone.getNumber();
            } else if (phone.getType().equals("Home") && phone.getNumber() == null) {
                data = "Sin agregar";
            }
        }
        return data;
    }

    public String getCellPhone() {

        String data = "";
        for (Phones phone : phones) {

            if (phone.getType().equals("Cellphone") && phone.getNumber() != null) {
                data = phone.getNumber();
            } else if (phone.getType().equals("Cellphone") && phone.getNumber() == null) {
                data = "Sin agregar";
            }
        }
        return data;

    }

    public String getWorkPhone() {

        String data = "";
        for (Phones phone : phones) {

            if (phone.getType().equals("Office") && phone.getNumber() != null) {
                data = phone.getNumber();
            } else if (phone.getType().equals("Office") && phone.getNumber() == null) {
                data = "Sin agregar";
            }
        }
        return data;

    }

    public String getHomeAddress() {
        String data = "";

        for (Addresses address : addresses) {
            if (address.getHome() != null) {
                data = address.getHome();

            } else {
                data = "Sin agregar";
            }
        }
        return data;

    }

    public String getWorkAddress() {
        String data = "";

        for (Addresses address : addresses) {
            if (address.getWork() != null) {
                data = address.getWork();

            } else {
                data = "Sin agregar";
            }
        }
        return data;

    }

}
