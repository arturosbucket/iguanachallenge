package com.arturo.iguanafix.Models;

import java.io.Serializable;

public class Phones implements Serializable {

    private String number;
    private String type;

    public String getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }
}
