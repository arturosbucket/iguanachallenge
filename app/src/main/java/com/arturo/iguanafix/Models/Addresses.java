package com.arturo.iguanafix.Models;

import java.io.Serializable;

public class Addresses implements Serializable {

    private String home;
    private String work;

    public String getHome() {
        return home;
    }

    public String getWork() {
        return work;
    }
}
